package es6;

import java.text.SimpleDateFormat;
import java.util.Date;


public class DataOra  implements Comparable<DataOra> {
	private int anno;
	private int mese;
	private int giorno;
	private int ore;
	private int minuti;
	private int secondi;
	private String Ex;


	public void setTempo(int ore,int minuti,int secondi) {
		if(ore>=1 && ore<24) this.ore=ore;
		if(minuti>=1 && minuti<60) this.minuti=minuti;
		if(secondi>=1 && secondi<60) this.secondi=secondi;
	}
	
	public int getAnno(){
		return anno;
	}

    public void setAnno(int anno)throws Exception{
    
         if(anno < 1500 || anno > 2500){
        	 Ex ="Errore! L'anno deve essere compreso fra 1500 e 2500";
        	 throw new RuntimeException();

        } 
             this.anno=anno;
}
    public int getMese(){
        return mese;             
    }
    public void setMese(int mese)throws Exception{
        if(mese < 1 || mese > 12){
            Ex = "il mese deve essere compreso fra 1 e 12";
            throw new RuntimeException();
        }
            this.mese=mese;
    }
    public int getGiorno(){
        return giorno;
    }
    public void setGiorno(int giorno){
    	if (mese != 2) {
    		
        if(giorno < 1 || giorno > 31){
        	Ex ="il giorno deve essere compreso fra 1 e 31";
            throw new RuntimeException();
        } 
    	}
        if (mese == 2){
        	
        	if(Bisestile(anno) == true) {
        	if(giorno < 1 || giorno > 28){
            	Ex ="il giorno deve essere compreso fra 1 e 28 perch� l'anno "+anno+" � bisestile!";
                throw new RuntimeException();
        }
        	}else {
        		if(giorno < 1 || giorno > 29){
                	Ex ="il giorno deve essere compreso fra 1 e 29 perch� l'anno "+anno+" non � bisestile!";
                    throw new RuntimeException();
        	}
        	}
        }
    	
        if(giorno == 31){
           switch (mese){
                case 2:
                case 4:
                case 6:
                case 9:
                case 11:
                	Ex ="il giorno 31 non esiste per il mese "+mese;
                    throw new RuntimeException();
        }
       }
        if(giorno == 30 && mese == 2){
        	Ex ="il giorno 30 non esiste per il mese di febbraio";
            throw new RuntimeException();
        }
        if(giorno == 29 && mese == 2){
            if(Bisestile(anno)){
            	Ex ="il giorno 30 non esiste per il mese di febbraio se non negli anni bisestili";
                throw new RuntimeException();
            }
        }
        this.giorno = giorno;
     
    }
    public int getOre(){
        return ore;
    }
    public void setOre(int ore){
         if(ore < 0 || ore > 24){
        	 Ex ="Errore! le ore devono essere comprese fra 0 e 24";
             throw new RuntimeException();
         }
        this.ore = ore;
    }
    public int getMinuti(){
        return minuti;
    }
    public void setMinuti(int minuti){
        if(minuti < 0 || minuti > 60){
        	Ex ="Errore! i minuti devono essere compresi fra 0 e 60";
            throw new RuntimeException();
        }
        this.minuti = minuti;
    }
    public int getSecondi(){
        return secondi;
    }
    public void setSecondi(int secondi){
        if(secondi < 0 || secondi > 60){
        	Ex ="Errore! i secondi devono essere compresi fra 0 e 60";
            throw new RuntimeException();
        }
    
        this.secondi = secondi;
    }
    
    
    
	public String getEx() {
		return Ex;
	}

	@Override
	public boolean equals(Object obj) {
		DataOra data2 = (DataOra) obj;
		if (anno != data2.anno || mese != data2.mese || giorno != data2.giorno || ore != data2.ore
				|| minuti != data2.minuti || secondi != data2.secondi) {
			return false;
	}else {
		return true;
	}
	}

	private boolean Bisestile(int anno) {
		if (anno % 400 == 0) {
			return true;
		}
		if (anno % 100 == 0) {
			return false;
		}
		if (anno % 4 == 0) {
			return true;
		}
		return false;
	}
//	public int getTime(Date d1,DataOra d2) {	
//	}
//	
	public void differenzaTempi(DataOra data1,DataOra data2) {
		try {
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			fmt.setLenient(false);
			Date d1 = fmt.parse(data1.stampaTutto());
			Date d2 = fmt.parse(data2.stampaTutto());
			long millisDiff = d1.getTime() - d2.getTime();
			int secondi = (int) (millisDiff / 1000 % 60);
			int minuti = (int) (millisDiff / 60000 % 60);
			int ore = (int) (millisDiff / 3600000 % 24);
			int giorno = (int) (millisDiff / 86400000);
			int mese = (int )(millisDiff / 262800288 /(1000));
			int anno = (int) (millisDiff / 31536000/(1000) );	
			System.out.println("\nFra " + data1.stampaTutto()+ " e " + data2.stampaTutto() + " ci sono: ");
			System.out.print(anno + " anni, ");
			System.out.print(mese + " mese, ");
			System.out.print(giorno + " giorni, ");
			System.out.print(ore + " ore, ");
			System.out.print(minuti + " minuti, ");
			System.out.println(secondi + " secondi");
			}catch(Exception e){
				System.out.println("Errore!"+e);
				
			}
	}
	
	public void stampData() {
		System.out.print("" + getAnno() + "/" + getMese() + "/" + getGiorno());
	}

	public void stampatempo() {
		System.out.println(" Tempo : " + getOre() + ":" + getMinuti() + ":" + getSecondi());
	}

	public String stampaTutto() {
		return "" + this.getAnno() + "/" + this.getMese() + "/" + this.getGiorno() + "   " + this.getOre() + ":"
				+ this.getMinuti() + ":" + this.getSecondi();
	}
	

	@Override
	public int compareTo(DataOra data2) {
		if (data2.anno != anno) {
			return anno - data2.anno;
		}
		if (data2.mese != mese) {
			return mese - data2.mese;
		}
		if (data2.giorno != giorno) {
			return giorno - data2.giorno;
		}
		if (data2.ore != ore) {
			return ore - data2.ore;
		}
		if (data2.minuti != minuti) {
			return minuti - data2.minuti;
		}
		if (data2.secondi != secondi) {
			return secondi - data2.secondi;
		}
		return 0;
	}
}