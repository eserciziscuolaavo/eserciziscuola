package es6;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Scanner;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class Main {
	public static void main(String[] args) throws Exception {
		Scanner input = new Scanner(System.in);
		boolean errore = false;
		int risposta = 0;

		DataOra data1 = new DataOra();
		DataOra data2 = new DataOra();
		System.out.println("Inserire le date da confrontare nel formato: [anni/mesi/giorni] " + "[ore/minuti/secondi]");
		System.out.println("inserire Data 1 [aaaa/mm/gg]: ");

		do {
			try {
				do {
					try {
						System.out.println("Inserisci l'anno");

						risposta = Integer.parseInt(input.nextLine());
						data1.setAnno(risposta);
						errore = false;
					} catch (NumberFormatException e) {
						System.out.println("Inserisci un valore numerico intero \n");
						errore = true;
					} catch (RuntimeException re) {
						System.out.println(data1.getEx());
						errore = true;
					}
				} while (errore);
				do {
					try {
						System.out.println("Inserisci il mese");
						risposta = Integer.parseInt(input.nextLine());
						data1.setMese(risposta);
						errore = false;
					} catch (NumberFormatException e) {
						System.out.println("Inserisci un valore numerico intero \n");
						errore = true;

					} catch (RuntimeException re) {
						System.out.println(data1.getEx());
						errore = true;
					}
				} while (errore);
				do {
					try {
						System.out.println("Inserisci il giorno");
						risposta = Integer.parseInt(input.nextLine());
						data1.setGiorno(risposta);
						errore = false;
					} catch (NumberFormatException e) {
						System.out.println("Inserisci un valore numerico intero \n");
						errore = true;

					} catch (RuntimeException re) {
						System.out.println(data1.getEx());
						errore = true;
					}
				} while (errore);
			} catch (NumberFormatException e) {
				System.out.println("Inserisci un valore numerico intero \n");
				errore = true;
			} catch (RuntimeException re) {
				System.out.println(data1.getEx());
				errore = true;
			}
		} while (errore);
		System.out.println("Data 1 Tempo[hh:mm:ss]: ");
		do {
			try {
				do {
					try {
						System.out.println("Inserisci le ore");
						risposta = Integer.parseInt(input.nextLine());
						data1.setOre(risposta);
						errore = false;
					} catch (NumberFormatException e) {
						System.out.println("Inserisci un valore numerico intero \n");
						errore = true;

					} catch (RuntimeException re) {
						System.out.println(data1.getEx());
						errore = true;
					}
				} while (errore);
				do {
					try {
						System.out.println("Inserisci i minuti");
						risposta = Integer.parseInt(input.nextLine());
						data1.setMinuti(risposta);
						errore = false;
					} catch (NumberFormatException e) {
						System.out.println("Inserisci un valore numerico intero \n");
						errore = true;

					} catch (RuntimeException re) {
						System.out.println(data1.getEx());
						errore = true;
					}
				} while (errore);
				do {
					try {
						System.out.println("Inserisci il secondi");
						risposta = Integer.parseInt(input.nextLine());
						data1.setSecondi(risposta);
						errore = false;
					} catch (RuntimeException re) {
						System.out.println(data1.getEx());
						errore = true;
					}
				} while (errore);
			} catch (NumberFormatException e) {
				System.out.println("Inserisci un valore numerico intero \n");
				errore = true;
			} catch (RuntimeException re) {
				System.out.println(data1.getEx());
				errore = true;
			}
		} while (errore);

		System.out.println("Inserire Data 2 [aaaa/mm/gg]: ");
		do {
			try {
				do {
					try {
						System.out.println("Inserisci l'anno");

						risposta = Integer.parseInt(input.nextLine());
						data2.setAnno(risposta);
						errore = false;
					} catch (NumberFormatException e) {
						System.out.println("Inserisci un valore numerico intero \n");
						errore = true;
					} catch (RuntimeException re) {
						System.out.println(data2.getEx());
						errore = true;
					}
				} while (errore);
				do {
					try {
						System.out.println("Inserisci il mese");
						risposta = Integer.parseInt(input.nextLine());
						data2.setMese(risposta);
						errore = false;
					} catch (NumberFormatException e) {
						System.out.println("Inserisci un valore numerico intero \n");
						errore = true;

					} catch (RuntimeException re) {
						System.out.println(data2.getEx());
						errore = true;
					}
				} while (errore);
				do {
					try {
						System.out.println("Inserisci il giorno");
						risposta = Integer.parseInt(input.nextLine());
						data2.setGiorno(risposta);
						errore = false;
					} catch (NumberFormatException e) {
						System.out.println("Inserisci un valore numerico intero \n");
						errore = true;

					} catch (RuntimeException re) {
						System.out.println(data2.getEx());
						errore = true;
					}
				} while (errore);
			} catch (NumberFormatException e) {
				System.out.println("Inserisci un valore numerico intero \n");
				errore = true;
			} catch (RuntimeException re) {
				System.out.println(data2.getEx());
				errore = true;
			}
		} while (errore);
		System.out.println("Data 2 Tempo[hh:mm:ss]: ");
		do {
			try {
				do {
					try {
						System.out.println("Inserisci le ore");
						risposta = Integer.parseInt(input.nextLine());
						data2.setOre(risposta);
						errore = false;
					} catch (NumberFormatException e) {
						System.out.println("Inserisci un valore numerico intero \n");
						errore = true;

					} catch (RuntimeException re) {
						System.out.println(data2.getEx());
						errore = true;
					}
				} while (errore);
				do {
					try {
						System.out.println("Inserisci i minuti");
						risposta = Integer.parseInt(input.nextLine());
						data2.setMinuti(risposta);
						errore = false;
					} catch (NumberFormatException e) {
						System.out.println("Inserisci un valore numerico intero \n");
						errore = true;

					} catch (RuntimeException re) {
						System.out.println(data2.getEx());
						errore = true;
					}
				} while (errore);
				do {
					try {
						System.out.println("Inserisci il secondi");
						risposta = Integer.parseInt(input.nextLine());
						data2.setSecondi(risposta);
						errore = false;
					} catch (RuntimeException re) {
						System.out.println(data2.getEx());
						errore = true;
					}
				} while (errore);
			} catch (NumberFormatException e) {
				System.out.println("Inserisci un valore numerico intero \n");
				errore = true;
			} catch (RuntimeException re) {
				System.out.println(data2.getEx());
				errore = true;
			}
		} while (errore);
		System.out.println("\n ");
		System.out.print("Data 1 ");
		data1.stampData();
		data1.stampatempo();
		System.out.print("Data 2 ");
		data2.stampData();
		data2.stampatempo();

		System.out.println("\nData 1 " + ((data1.equals(data2) == true) ? "" : "non") + " � uguale a Data 2");

		int confronto = data1.compareTo(data2);
		String operatore;
		if (confronto < 0) {
			operatore = "< di";
		} else if (confronto == 0) {
			operatore = "= a";
		} else {
			operatore = "> di";
		}
		input.close();
		System.out.print("Data 1 [");
		data1.stampData();
		System.out.print("] � " + operatore + " Data 2 [");
		data2.stampData();
		System.out.print("]\n");

		data1.differenzaTempi(data1, data2);

		String[] ids = TimeZone.getAvailableIDs(60 * 60 * 1000);

		if (ids.length == 0)
			System.exit(0);

		System.out.println("\nCurrent Time");

		SimpleTimeZone pdt = new SimpleTimeZone(60 * 60 * 1000, ids[0]);

		pdt.setStartRule(Calendar.JANUARY, 1, Calendar.MONDAY, 2 * 60 * 60 * 1000);
		pdt.setEndRule(Calendar.DECEMBER, -1, Calendar.MONDAY, 2 * 60 * 60 * 1000);
		Calendar calendar = new GregorianCalendar(pdt);
		Date trialTime = new Date();
		calendar.setTime(trialTime);
		System.out.println(trialTime);

		System.out.println("WEEK_OF_YEAR: " + calendar.get(Calendar.WEEK_OF_YEAR));
		System.out.println("WEEK_OF_MONTH: " + calendar.get(Calendar.WEEK_OF_MONTH));
		System.out.println("DATE: " + calendar.get(Calendar.DATE));
		System.out.println("DAY_OF_MONTH: " + calendar.get(Calendar.DAY_OF_MONTH));
		System.out.println("DAY_OF_YEAR: " + calendar.get(Calendar.DAY_OF_YEAR));
		System.out.println("DAY_OF_WEEK: " + calendar.get(Calendar.DAY_OF_WEEK));
		System.out.println("DAY_OF_WEEK_IN_MONTH: " + calendar.get(Calendar.DAY_OF_WEEK_IN_MONTH));

	}
}