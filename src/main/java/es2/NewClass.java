
package es2;

    public class NewClass {

    public static void main(String[] args) {
		System.out.println("Type = " + Byte.TYPE+" size = "+ Byte.SIZE+" byte = "+Byte.BYTES+" maxValue = "+ Byte.MAX_VALUE+" minValue = "+ Byte.MIN_VALUE);
		System.out.println("Type = " + Character.TYPE+" size = "+  Character.SIZE+" byte = "+ Character.BYTES+" maxValue = "+  (int) Character.MAX_VALUE+" minValue = "+ (int) Character.MIN_VALUE);
		System.out.println("Type = " + Integer.TYPE+" size = "+ Integer.SIZE+" byte = "+Integer.BYTES+" maxValue = "+ (int)Integer.MAX_VALUE+" minValue = "+ (int)Integer.MIN_VALUE);
		System.out.println("Type = " + Float.TYPE+" size = "+ Float.SIZE+" byte = "+Float.BYTES+" maxValue = "+ (float)Float.MAX_VALUE+" minValue = "+ (float)Float.MIN_VALUE);
		System.out.println("Type = " + Short.TYPE+" size = "+ Short.SIZE+" byte = "+Short.BYTES+" maxValue = "+ (short)Short.MAX_VALUE+" minValue = "+ (short)Short.MIN_VALUE);
		System.out.println("Type = " + Long.TYPE+" size = "+ Long.SIZE+" byte = "+Long.BYTES+" maxValue = "+ (long)Long.MAX_VALUE+" minValue = "+ (long)Long.MIN_VALUE);	
	}    
}
    
  