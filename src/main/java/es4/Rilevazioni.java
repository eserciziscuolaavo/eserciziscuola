package es4;

import java.util.Scanner;

public class Rilevazioni {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int scelta;
        float max = 59.8f;
        float min = -253.15f;
        float rilevazioni = 0;
        float valore = 0;
        int i, j= 0;
        boolean errore = false;
        boolean controlloTemperatura = true;
        int numeriTemperatura = 0;
        float arraySettimana[][] = new float[7][];
        String st = null;

        String giorni[] = new String[7];
        giorni[0] = "Lunedì";
        giorni[1] = "Martedì";
        giorni[2] = "Mercoledì";
        giorni[3] = "Giovedì";
        giorni[4] = "Venerdì";
        giorni[5] = "Sabato";
        giorni[6] = "Domenica";

        for (i = 0; i < arraySettimana.length; i++) {
            do {
                System.out.println("Quante rilevazioni vuoi inserire nel giorno di " + (giorni[i] + ":"));

                try {
                    st = input.nextLine();
                    if (st.equals("")) {
                        rilevazioni = 0;
                        errore = true;
                    } else {
                        rilevazioni = Float.parseFloat(st);
                        errore = false;
                    }

                } catch (NumberFormatException ex) {
                    System.out.println("Inserisci un valore intero positivo!\n");
                    errore = true;
                }
                try {
                    arraySettimana[i] = new float[(int) rilevazioni];
                } catch (NegativeArraySizeException e) {
                    System.out.println("Un array non può avere valori negativi!\n");
                    rilevazioni = 0;
                    errore = true;
                }
            } while (errore);

            for (j = 0; j < rilevazioni; j++) {
                if (!controlloTemperatura) {
                    j--;
                    controlloTemperatura = true;
                }
                do {
                    try {

                        System.out.println("Inserisci temperatura " + (j + 1) + ":");
                        st = input.nextLine();
                        valore = Float.parseFloat(st);
                        errore = false;

                    } catch (NumberFormatException ex) {
                        System.out.println("Il valore [" + st + "] non può essere una temperatura\n");
                        errore = true;
                    }

                    if ((valore > min) && (valore < max)) {
                        arraySettimana[i][j] = valore;
                    } else {
                        System.out.println("ERRORE: la temperatura deve essere compresa tra " + min + " e " + max);
                        valore = 0;
                        controlloTemperatura = true;
                        j--;
                    }

                } while (errore);
                arraySettimana[i][j] = valore;
            }
        }
        do {
            System.out.println("----------------------------------------------------------------------------------------------");
            System.out.println("-1 Visualizza tutti i valori");
            System.out.println("-2 Visualizzare i valori di un particolare giorno della settimana nell'ordine di inserimento");
            System.out.println("-3 Visualizzare i valori di un particolare giorno della settimana per valori crescenti");
            System.out.println("-4 Exit");
            System.out.println("----------------------------------------------------------------------------------------------");
            System.out.println();
            try {
                st = input.next();
                scelta = Integer.parseInt(st);
            } catch (NumberFormatException ex) {
                scelta = 0;
            }
            switch (scelta) {
                case 1:
                    for (i = 0; i < arraySettimana.length; i++) {
                        System.out.print("Stampo le temperature di " + giorni[i] + " sono :");
                        if (arraySettimana[i].length == 0) {
                            System.out.print("Non ci sono temperature da visualizzare!");
                        }
                        for (j = 0; j < arraySettimana[i].length; j++) {
                            System.out.print("[" + arraySettimana[i][j] + "°] ");
                        }
                        System.out.println();
                    }
                    System.out.println();
                    break;

                case 2:
                    do {
                        try {
                            for (i = 0; i < giorni.length; i++) {
                                System.out.print((i + 1) + ") " + giorni[i] + "  ");
                            }
                            System.out.println("\n");
                            errore = true;
                            while (errore) {
                                System.out.print("Scegli il giorno della settimana che vuoi visualizzare :  ");//vorrei far capire che per selezionare il giorno voluto bisogna premere un numero
                                try {
                                    errore = false;
                                    st = input.next();
                                    scelta = Integer.parseInt(st);
                                } catch (NumberFormatException ex) {
                                    System.out.println("\nHai inserito un valore non corretto! \n");
                                    errore = true;
                                }
                            }

                            System.out.println();
                            scelta = scelta - 1;
                            System.out.print("Stampo temperature di " + giorni[scelta] + ":");
                            if (arraySettimana[scelta].length == 0) {
                                System.out.print("Non ci sono temperature da stampare!");
                            }
                            for (j = 0; j < arraySettimana[scelta].length; j++) {
                                System.out.print("[" + arraySettimana[scelta][j] + "°] ");
                            }
                            errore = false;
                        } catch (ArrayIndexOutOfBoundsException e) {
                            System.out.println("Ci sono 7 giorni! Inserisci un valore corretto da 1 a 7");
                            errore = true;
                        }
                        System.out.println("\n");
                    } while (errore);
                    break;
                case 3:
                    do {
                        try {
                            for (i = 0; i < giorni.length; i++) {
                                System.out.print((i + 1) + ") " + giorni[i] + " ");
                            }
                            System.out.println("\n");
                            errore = true;
                            while (errore) {
                                System.out.print("Scegli il giorno della settimana che vuoi visualizzare :  ");
                                try {
                                    errore = false;
                                    st = input.next();
                                    scelta = Integer.parseInt(st);
                                } catch (NumberFormatException ex) {
                                    System.out.println("\nHai inserito un valore non corretto! \n");
                                    errore = true;
                                }
                            }
                            System.out.println();
                            scelta = scelta - 1;
                            float temp;
                            for (i = 0; i < arraySettimana[scelta].length; i++) {

                                boolean scambio = false;
                                for (j = 0; j < arraySettimana[scelta].length - 1; j++) {
                                    if (arraySettimana[scelta].length == 0) {
                                        System.out.print("Non ci sono temperature da ordinare!");
                                    }

                                    do {
                                        if (arraySettimana[scelta][j] > arraySettimana[scelta][j + 1]) {
                                            temp = arraySettimana[scelta][j];
                                            arraySettimana[scelta][j] = arraySettimana[scelta][j + 1];
                                            arraySettimana[scelta][j + 1] = temp;
                                            scambio = true;
                                        } else {
                                            scambio = true;
                                        }
                                    } while (!scambio);
                                }

                            }
                            errore = false;
                        } catch (ArrayIndexOutOfBoundsException e) {
                            System.out.println("I giorni sono 7! Inserisci un valore corretto da 1 a 7");
                            errore = true;
                        }
                        System.out.println();
                        System.out.println("----------------------------------------------------------------------------------------------\n");
                    } while (errore);

                    for (i = 0; i < arraySettimana[scelta].length; i++) {
                        System.out.println(arraySettimana[scelta][i]);
                    }
                    break;
                case 4:
                    System.out.println("Programma terminato!!!");
                    input.close();
                    break;

                default:
                    if (scelta != 1 || scelta != 2 || scelta != 3 || scelta != 4) {
                        System.out.print("Errore! Per navigare nel menù scegli un numero da 1 a 4! \n");
                        System.out.println("Riprova");
                        errore = true;
                    }
            }
        } while (4 != scelta);

    }
}
