package es11;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Verifica {

    public static String leggiStringa(String prompt) {
        Scanner reader = new Scanner(System.in);
        System.out.println(prompt);
        return reader.nextLine();
    }

    public static int leggiIntero(String prompt, Integer min, Integer max) {
        Scanner reader = new Scanner(System.in);
        while (true) {
            System.out.println(prompt);
            String risposta = reader.nextLine();
            try {
                int num = Integer.parseInt(risposta);
                if (min != null) {
                    if (num < min) {
                        throw new RuntimeException("Il numero deve essere maggiore di " + min);
                    }
                }
                if (max != null) {
                    if (num > min) {
                        throw new RuntimeException("Il numero deve essere minore di " + max);
                    }
                }
            } catch (Exception ex) {
                System.out.println("\n ERRORE: " + ex.getMessage());
            }
        }

    }

    static void elenco(Map<String, File> fileAliases) {
        System.out.println("Elenco degli Alias/File");
        if (fileAliases.isEmpty()) {
            System.out.println("Non esiste alcun Alias/File");
        }
        for (String alias : fileAliases.keySet()) {
            System.out.println(alias + " ----> " + fileAliases.get(alias).getPath());
        }
    }

    static void inserimento(Map<String, File> fileAliases) {
        System.out.println("Inserimento di un nuovo alias");
        String nome = leggiStringa("Inserisci l'alias del file: ");
        String path = leggiStringa("Inserisci il path assoluto o relativo del file: ");
        File f = new File(path);

        File oldFile = fileAliases.get(nome);

        if (oldFile != null) {
            System.out.println("Esiste già il File " + oldFile.getPath());
            String risposta = leggiStringa("Vuoi sostituirlo? ");
            if (risposta.equalsIgnoreCase("no")) {
                System.out.println("Inserimento annullato!");
                return;
            }
        }
        fileAliases.put(nome, f);

        if (oldFile != null) {
            System.out.println("Il vecchio file e' stato sostituito!");
        } else {
            System.out.println("il nuovo alias e' stato inserito!");
        }
    }

    static void elimina(Map<String, File> fileAliases) {
        System.out.println("Eliminazione di un alias esistente");
        String alias = leggiStringa("Inserisci l'alias del file da eliminare: ");
        File oldFile = fileAliases.get(alias);
        if (oldFile == null) {
            System.out.println("Non esiste il File associato all'alias");
            return;
        }
        String risposta = leggiStringa("Confermi l'eliminazione? ");
        if (risposta.equalsIgnoreCase("no")) {
            System.out.println("Eliminazione annullata!");
            return;
        }

        fileAliases.remove(alias);
    }

    static void operazioni(Map<String, File> fileAliases) {
        String alias = leggiStringa("Scegli l'alias su cui fare operazioni: ");
        File f = fileAliases.get(alias);
        if (f == null) {
            System.out.println("L'alias " + alias + " non ha un file associato!");
            return;
        }

        System.out.println("Hai scelto di eseguire operazioni sul file " + f.getPath());
        menuOperazioni(f);
    }

    private static void menuOperazioni(File f) {
        while (true) {
            System.out.println("\n\n___________________________________");
            System.out.println("Operazioni sul file " + f.getPath());
            System.out.println("");
            System.out.println("1) Verifica esistenza");
            System.out.println("2) Dettagli del File");
            System.out.println("3) Dettagli dell Directory");
            System.out.println("4) Creazione del File /Alias");
            System.out.println("5) Eliminazione del File /Alias");
            System.out.println("6) Rinomina del File");
            System.out.println("7) Ritorno al menu' principale");
            System.out.print("Scelta: ");

            int scelta = leggiIntero("Scelta: ", 1, 6);

            switch (scelta) {
                case 1: {
                    if (f.exists()) {
                        System.out.println("Il percorso esiste!");
                    } else {
                        System.out.println("Il percorso NON esiste! ");
                    }
                }
                break;

                case 2: {
                    if (!f.exists()) {
                        System.out.println("Non è possinile avere dettagli perche' il percorso NON esiste");
                        break;
                    }
                    dettagli(f);
                    break;
                }

                case 3: {

                    if (!f.exists()) {
                        System.out.println("Il percorso non esiste");
                        return;
                    }

                    if (!f.isDirectory()) {
                        System.out.println("Non posso visualizzare i file del percorso perche'");
                        return;

                    }
                    File[] contenuto = f.listFiles();
                    if (contenuto == null || contenuto.length == 0) {
                        System.out.println("La directory e' vuota");
                    }
                    for (File f2 : contenuto) {
                        dettagli(f2);
                    }
                }

                case 4: {
                    if (f.exists()) {
                        System.out.println("non è possibile effettuare la creazuibe perche' il percorso esiste gia'!");
                        return;
                    }
                    String risposta = leggiStringa("Scegli se creare un file o una directory (f / d): ");
                }
                break;

                case 5: {
                    if (!f.exists()) {
                        System.out.println("Non è possinile avere dettagli perche' il percorso NON esiste");
                        break;
                    }

                    String risposta = leggiStringa("Confermi cancellazione? ");
                    eliminaFile(f);
                    if (f.isFile()) {
                        if (f.delete()) {
                            System.out.println("Il file e' stato cancellato");
                        } else {
                            System.out.println("il file NON e' stato cancellato");
                        }
                    }

                    //è directory
                    File[] contenuto = f.listFiles();
                    if (contenuto == null || contenuto.length == 0) {
                        System.out.println("La directory e' vuota");
                        f.delete();
                    }

                }
                break;

                case 6: {
                    return;
                }
            }

        }
    }

    public static void main(String[] args) {
        Map<String, File> fileAliases = new HashMap();
        while (true) {
            System.out.println("1) Elenco alias disponibili");
            System.out.println("2) Inserimento nuovo alias");
            System.out.println("3) Eliminare un alias esistente");
            System.out.println("4) ");
            System.out.println("5) Fine programma");
            System.out.print("Scelta: ");

            int scelta = leggiIntero("Scelta: ", 1, 5);

            switch (scelta) {
                case 1: {
                    elenco(fileAliases);
                }
                break;

                case 2: {
                    inserimento(fileAliases);
                }
                break;

                case 3: {
                    elimina(fileAliases);
                }
                break;

                case 4: {
                    operazioni(fileAliases);
                }
                break;

                case 5: {
                    System.exit(0);
                }
                break;
            }

        }

    }

    private static void dettagli(File f) {
        System.out.println("Percorso assoluto: " + f.getAbsolutePath());
        String canonical = "No info";
        try {
            canonical = f.getCanonicalPath();
        } catch (IOException ex) {
            System.out.println("Errore nel getCanonical path: " + ex.getMessage());
        }
        System.out.println("Percorso assoluto: " + canonical);
        long lastUpdate = f.lastModified();
        SimpleDateFormat fmt = new SimpleDateFormat("d/M/Y HH:mm:ss");
        System.out.println("Ultima modifica il: " + fmt.format(new Date(lastUpdate)));
        System.out.println("Dimensione in byte" + f.length());
    }

    private static void eliminaFile(File f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
