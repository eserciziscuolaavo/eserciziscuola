package es10;
import java.util.Scanner;

public class Directory {
	private String path = null;
	static Scanner input = new Scanner(System.in);

	public Directory(String path) {
		this.setPath(path);
	}
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		path = input.nextLine();
		this.path = path;
	}
}

