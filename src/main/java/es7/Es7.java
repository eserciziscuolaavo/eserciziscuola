package es7;

import java.util.List;
import java.util.Scanner;

public class Es7 {

    public static void main(String[] args) {
    	int base = 0;
    	int altezza = 0;
    	boolean bool = false;
		Scanner reader = new Scanner(System.in);
		System.out.println("\nEsercizio 7");
		
		
		List<Rettangolo> ListaRettangoli = new Cartesiano<Rettangolo>(2);

		
		int scelta;
		
		do {
		System.out.println();
		System.out.println("----------------------------------------------------------------------------------------------\n");
		System.out.println("1) Aggiungi rettangoli alla lista");
		System.out.println("2) Visualizza grandezza lista");
		System.out.println("3) Visualizza contenuto lista");
		System.out.println("4) Elimina tutto il contenuto della lista");
		System.out.println("5) Esci dal programma\n");
		System.out.print("Scelta: ");
		scelta = Integer.parseInt(reader.nextLine());
		System.out.println("----------------------------------------------------------------------------------------------\n");

		switch (scelta) {
		
		case 1: {
		while(bool=true) {
			String risposta = null;
			System.out.println("Inserisci i dati del rettangolo:");
			
			
			do {
			try {
			System.out.print("base:");
			base = Integer.parseInt(reader.nextLine());
			bool = false;
			if(base<0) {
				System.out.println("Inserisci un valore intero positivo, la base non pu� essere negativa\n");
				bool = true;
			}
			}catch(NumberFormatException ex) {
				System.out.println("Hai inserito un valore non corretto\n");
				bool = true;
			}
			}while(bool);
			
			
			do {
				try {
			System.out.print("altezza:");
			altezza = Integer.parseInt(reader.nextLine());
			bool = false;
			if(altezza<0) {
				System.out.println("Inserisci un valore intero positivo, l'altezza non pu� essere negativa\n");
				bool = true;
			}
			}catch(NumberFormatException ex) {
				System.out.println("Hai inserito un valore non corretto\n");
				bool = true;
			}
			}while(bool);
			
			System.out.print("Vuoi inserire le coordinate e il colore? (S/N) (OPZIONALE):");
			risposta = reader.nextLine().toUpperCase();
			Integer x = null;
			Integer y = null;
			Colore colore = null;
			if (risposta.equalsIgnoreCase("S")) {
				
				
				do {
				try {
			System.out.print("Coordinata x:");
			x = Integer.parseInt(reader.nextLine());
			bool = false;
				}catch(NumberFormatException ex) {
					System.out.println("Hai inserito un valore non corretto\n");
					bool = true;
				}
				}while(bool);
				
				
				do {
					try {
				
			System.out.print("Coordinata y:");
			y = Integer.parseInt(reader.nextLine());
				bool = false;
					}catch(NumberFormatException ex) {
						System.out.println("Hai inserito un valore non corretto\n");
						bool = true;
					}
					}while(bool);
				
				
				do {
					try {
				
			System.out.print("Scrivi il nome del colore tra ROSSO,VERDE,NERO,GIALLO:");
			colore = Colore.valueOf(reader.nextLine().toUpperCase());
			bool = false;
					}catch(Exception ex) {
						System.out.println("Scrivi il nome del colore in modo corretto!\n");
						bool = true;
					}
					}while(bool);
			}
			Rettangolo r = new Rettangolo(base, altezza, x,y,colore);
			
			
			ListaRettangoli.add(r);
			
			
			while (true) {
				System.out.print("Vuoi aggiungere altri rettangoli ? (S/N) ");
				risposta = reader.nextLine().toUpperCase();
				if (risposta.equalsIgnoreCase("S")
				||  risposta.equalsIgnoreCase("N"))
					break;
			}
			if (risposta.equalsIgnoreCase("N")) {
				break;
			}
		}
		}
		break;
		
		case 2: {
			System.out.println("Grandezza lista Rettangolo: ["+ListaRettangoli.size()+"]");
			break;
		}
		
		case 3: {
			if(ListaRettangoli.size()==0) {
				System.out.println("La lista � vuota");
			}else {
			for (int i = 0; i < ListaRettangoli.size(); i++) {
				System.out.println("Rettangoli:"+ListaRettangoli.get(i));
			}
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
			break;
			}
		
		case 4: {

			if(ListaRettangoli.size()==0) {
				System.out.println("La lista � vuota, non c'� nulla da eliminare!");
			}else {
			System.out.print("Eliminazione in corso");
			for (int i = 0; i < 3; i++) {
			    try {
			        Thread.sleep(800);
			    } catch (Exception e) {
			        System.out.println(e);
			    }
			    System.out.printf(".", (i+1) );
			}
			ListaRettangoli.clear();
			Rettangolo.setContaRettangoli(0);
			try {
				Thread.sleep(1200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("\nEliminazione completata!");
		}
			break;
		}
		
		case 5:{
			System.out.println("Programma terminato!");
			System.exit(0);
		}
		default:
			System.out.println("Errore! Per navigare nel men� scegli un numero da 1 a 5!");
			break;
		}
		
		}while (scelta != 5);
    }
    
}

