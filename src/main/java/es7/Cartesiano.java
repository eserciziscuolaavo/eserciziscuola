package es7;


public class Cartesiano<E> extends CartesianoBasic<E>{
	
	
	private Object[] vettore;
	private int  numElementi;
	private int initialCapacity;
	
	public Cartesiano(int initialCapacity) {
		this.initialCapacity = initialCapacity;
		numElementi = 0;
		vettore = new Object[initialCapacity];
	}
	
    
        @Override
    public boolean add(E e) {
        	if (numElementi >= vettore.length) {
    			Object nuovoVettore[] = new Object[vettore.length+initialCapacity];
    			for (int i = 0; i < vettore.length; i++) {
    				nuovoVettore[i] = vettore[i];
    			}
    			vettore = nuovoVettore;
    		}
    		vettore[numElementi] = e;
    		numElementi++;
    		return true;
    	
    }
    
        @Override
    public int size() {
        return numElementi;
    }
    
    
    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
        @Override
	        public void clear() {
        	for (int i = 0; i < vettore.length; i++)
                vettore[i] = null;

            numElementi = 0;
    }

    @Override
    public E get(int index) {
    	if (index < 0 || index >= size()) {
			return null;

		}
		return (E) vettore[index];
    }
    
    @Override
    public int indexOf(Object o) {
    	if (o == null) {
            for (int i = 0; i < vettore.length; i++)
                if (vettore[i]==null)
                    return i;
        } else {
            for (int i = 0; i < vettore.length; i++)
                if (o.equals(vettore[i]))
                    return i;
        }
        return -1;

    }
    
}
