package es7;

public class Rettangolo {
    private static int contaRettangoli = 0;
    private int posizione;
    private int base;
    private int altezza;
    private int top;
    private int left;
    private Colore colore;

public Rettangolo (int base, int altezza, Integer top, Integer left, Colore colore){
    posizione = ++contaRettangoli;
    
    this.base = base;
    this.altezza = altezza;
    
    if (top == null){
        top = 0;
    }
    this.top = top;
    
    if (left == null){
        left = 0;
    }
    this.left = left;
    
    if (colore == null){
        colore = Colore.NERO;
    }
    this.colore = colore;
}

public void setBase(int base) throws Exception{
    if(base<0){
    throw new Exception("La base deve essere positiva");
    }
    this.base = base;
}

public void setAltezza(int altezza) throws Exception{
    if(altezza<0){
    throw new Exception("L'altezza deve essere positiva");
    }
    this.altezza = altezza;
}

public void setTop(int top){
    this.top = top;
}

public void setLeft(int left){
    this.left = left;
}

public void setColore(Colore colore){
    this.colore = colore;
}

public int getPosizione(){
    return posizione;
}

public static int getContaRettangoli() {
	return contaRettangoli;
}

public static void setContaRettangoli(int contaRettangoli) {
	Rettangolo.contaRettangoli = contaRettangoli;
}

public int getArea(){
    return base*altezza;
}

public int getPerimetro(){
    return (base+altezza)*2;
}

public Coordinate getTopLeft(){
    return new Coordinate (left,top);
}

public Coordinate getTopRight(){
    return new Coordinate (left+base,top);
}

public Coordinate getBottomRight(){
    return new Coordinate (left+base,top-altezza);
}

public Coordinate getBottomLeft(){
    return new Coordinate (left,top-altezza);
}

@Override
public String toString() {
	return "\nRettangolo {" + "pos=" + posizione + "�, base=" + base + 
			", altezza=" + altezza + ", colore=" + colore + 
			", coordinate Top,Left="  + getTopLeft().getTopLeft() + 
			", coordinate Top,Right="  + getTopRight().getTopRight() +
			", coordinate Bottom,Left="  + getBottomLeft().getBottomLeft() +
			", coordinate Bottom,Right="  + getBottomRight().getBottomRight() +'}';
}

}


