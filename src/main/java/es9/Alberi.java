package es9;

import java.util.Scanner;

import es7.Rettangolo;

public class Alberi {
	public static void main(String args[]) {
		boolean errore = true;
		
		while (errore) {
			String risposta = leggi("\t(I)Inserisci (O)VisualizzaOrdinaCrescente (D)VisualizzaOrdinaDecrescente (C)Cerca (E)Elimina (T)termina il programma: ").toUpperCase().trim();

			if (risposta.length() == 0) {
				continue;
			}

			char scelta = risposta.charAt(0);

			switch (scelta) {
			case 'I':
				aggiungiNodo();
				break;
			case 'C':
				cercaNodo();
				break;
			case 'E':
				eliminaNodo();
				break;
			case 'O':
				visualizzaOrdineAlberoCrescente(Nodo.radice);
				break;
			case 'D':
				VisualizzaordinaAlberoDescrescente(Nodo.radice);
				break;
			case 'T':
				terminaAlbero(Nodo.radice);
			}
		}
	}

	private static void aggiungiNodo() {
		boolean errore = true;
		do {
			try {
				int numero = Integer.parseInt(leggi("Scegli il valore da inserire nel nuovo nodo:"));
				Nodo nuovo = new Nodo();
				nuovo.numero = numero;
				if (Nodo.radice == null) {
					Nodo.radice = nuovo;
					return;
				}
				addNodo(Nodo.radice, nuovo);
			} catch (NumberFormatException ex) {
				System.out.println("ERRORE! il carattere non valido riprova!!!");
				errore = true;
			}
		} while (!errore);
	}

	private static void cercaNodo() {
		boolean errore = true;
		do {
			try {
				if (Nodo.radice == null) {
					System.out.println("L'albero è vuoto.");
					return;
				}

				int numero = Integer.parseInt(leggi("Scegli il valore del nodo da cercare:"));

				Nodo daCercare = new Nodo();
				daCercare.numero = numero;

				Nodo nodo = searchNodo(Nodo.radice, daCercare);

				if (nodo == null) {
					System.out.println("Il nodo non esiste !");
					return;
				}
				System.out.println("Il nodo esiste !");
			} catch (NumberFormatException ex) {
				System.out.println("ERRORE! il carattere non valido riprova!!!!");
			}
		} while (!errore);

	}

	static Nodo searchNodo(Nodo currNodo, Nodo daCercare) {
		if (currNodo == null) {
			return null;
		}

		if (daCercare.numero == currNodo.numero)
			return currNodo;

		if (daCercare.numero < currNodo.numero)
			return searchNodo(currNodo.sx, daCercare);

		return searchNodo(currNodo.dx, daCercare);

	}

	private static void eliminaNodo() {
		boolean errore = false;
		do {
			try {
				if (Nodo.radice == null) {
					System.out.println("L'albero è vuoto!!!");
					return;
				}

				int numero = Integer.parseInt(leggi("Scegli il valore del nodo da eliminare :"));
				if (numero == Nodo.radice.numero) {
					System.out.println("Il nodo da eliminare è la radice");
					Nodo.radice = null;
					return;
				}

				Nodo daCercare = new Nodo();
				daCercare.numero = numero;

				Nodo parent = searchParent(Nodo.radice, daCercare);
			
				if (parent == null) {
					System.out.println("Il nodo da eliminare non è presente nell'albero");
					return;
				}
				Nodo daEliminare;
				if (parent.sx.numero == daCercare.numero) {
					daEliminare = parent.sx;
					parent.sx = parent.sx.sx;
				} else {
					daEliminare = parent.dx;
					parent.dx = parent.dx.sx;
				}
				addNodo(parent, daEliminare.dx);
				break;
			} catch (NumberFormatException ex) {
				System.out.println("ERRORE! il carattere non valido riprova!!!!");
			}catch(NullPointerException ex) {
				System.out.println("ERRORE! il carattere non valido riprova!!!!");
			}
		} while (!errore);

	}

	static Nodo searchParent(Nodo parent, Nodo daCercare) {
		if (daCercare == null) {
			return null;
		}

		if (daCercare.numero == parent.sx.numero || daCercare.numero == parent.dx.numero)
			return parent;

		if (daCercare.numero < parent.numero)
			return searchNodo(parent.sx, daCercare);

		return searchNodo(parent.dx, daCercare);
	}

	static void addNodo(Nodo padre, Nodo nuovo) {
		if (nuovo == null)
			return;

		if (padre.numero == nuovo.numero) {
			System.out.println("Il valore esiste già nell'albero NON PUO' ESSERE INSERITO");
			return;
		}
		if (nuovo.numero < padre.numero) {
			if (padre.sx == null) {
				padre.sx = nuovo;
				return;
			}
			addNodo(padre.sx, nuovo);
		} else {
			if (padre.dx == null) {
				padre.dx = nuovo;
				return;
			}
			addNodo(padre.dx, nuovo);
		}
	}

	static void visualizzaOrdineAlberoCrescente(Nodo nodo) {
		if (nodo == null)
			return;
		visualizzaOrdineAlberoCrescente(nodo.sx);
		System.out.println(nodo.numero);
		visualizzaOrdineAlberoCrescente(nodo.dx);
	}
	
	
	public static void VisualizzaordinaAlberoDescrescente(Nodo nodo) {
		if (nodo == null)
			return;
		VisualizzaordinaAlberoDescrescente(nodo.dx);
		System.out.println(nodo.numero);
		VisualizzaordinaAlberoDescrescente(nodo.sx);

	}


	public static String leggi(String messaggioPerUtente) {
		System.out.print(messaggioPerUtente);
		Scanner reader = new Scanner(System.in);
		return reader.nextLine();
	}

	static public void terminaAlbero(Nodo nodo) {
		System.out.print("Termine dall'applicazione in corso");
		for (int i = 0; i < 3; i++) {
			try {
				Thread.sleep(800);
			} catch (Exception e) {
				System.out.println(e);
			}
			System.out.printf(".", (i + 1));
		}
		try {
			Thread.sleep(1200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("\nTermine applicazione completata!");
		System.exit(0);

	}
}
