package es8;

import java.util.Scanner;

public class Switch {

	Scanner input = new Scanner(System.in);
	Case Case = new Case();

	public void getSwitch() {

		System.out.print("Scelta: ");
		String risposta = input.nextLine();
		System.out.println(
				"\n----------------------------------------------------------------------------------------------\n");
		int scelta = 0;
		try {
			scelta = Integer.parseInt(risposta);

		} catch (NumberFormatException e) {
			scelta = -1;
		}

		switch (scelta) {
		case 1:
			Case.getCase1();
			break;

		case 2:
			Case.getCase2();
			break;

		case 3:
			Case.getCase3();
			break;

		case 4:
			Case.getCase4();
			break;

		case 5:
			Case.getCase5();
			break;

		case 0:
			System.out.println("Programma terminato!");
			System.exit(0);
			break;

		default:
			System.out.println("Errore! Per navigare nel menu' scegli un numero da 0 a 5!\n");
			break;
		}
	}
}
