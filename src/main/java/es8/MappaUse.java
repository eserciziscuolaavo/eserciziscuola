package es8;

import java.util.Collection;

public class MappaUse<K> extends Mappa<K> {

	private Object[] vettore;
	private int numElementi;

	@Override
	public void clear() {
		for (int i = 0; i < vettore.length; i++)
			vettore[i] = null;
		numElementi = 0;
	}

	@Override
	public int indexOf(Object o) {
		if (o == null) {
			return -1;
		}
		for (int i = 0; i < vettore.length; i++) {
			if (o.equals(vettore[i]))
				return i;
		}
		return -1;
	}

	@Override
	public boolean remove(Object key) {
		return super.remove(key);
	}

	@Override
	public K get(int index) {
		if (index < 0 || index >= size()) {
			return null;
		}
		return (K) vettore[index];
	}

	@Override
	public int size() {
		return numElementi;
	}

	@Override
	public boolean equals(Object o) {
		return super.equals(o);
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

}
