package es8;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class Case {

	Map<Integer, String> mappa = new HashMap<Integer, String>();

	boolean errore = true;
	Scanner input = new Scanner(System.in);
	String risposta;
	int scelta = 0;
	String value;
	Integer key;
	String lunghezzaStringa="0,1,2,3";

	public void getCase1() {

		do {
			try {
				do {
					System.out.println("Scegli la posizione della classifica ");
					risposta = input.nextLine();
					scelta = Integer.parseInt(risposta);

					if (scelta < 1) {
						System.out.println("Errore! Inserisci un valore positivo! La classifica inizia da 1\n");
					}
				} while (scelta < 1);
				Integer key = Integer.parseInt(risposta);
				String oldValue = mappa.get(key);

				if (oldValue != null) {
					do {
						errore = true;
						System.out.println("Questa posizione � gi� assegnato alla squadra [" + oldValue + "]");
						System.out.println("Vuoi sostituirlo?? S/N");
						risposta = input.nextLine().toUpperCase();
						if (risposta.equals("S") || (risposta.equals("N")))
							errore = false;
					} while (errore);
					if (risposta.equals("N")) {
						break;
					}
				}

				do {
					try {
					System.out.println("Inserisci la squadra da abbinare alla posizione");
					value = (input.nextLine().toUpperCase());
					
					value = value.replaceAll("[0-9.,]", " ");
					if (value.equals("")) {
						System.out.println("Errore! Inserimento non valido!");
					}
					}catch(NumberFormatException ex) {
						System.out.println("Errore! inserimento non valido!");
					}
				} while (value.equals(""));
				do {
				try {
					if(value.length() < 4) {	
						System.out.println("Ecco il taglio "+value.substring(0,4));
						System.out.println("Errore! inserimento sbagliato");
						errore=false;
					}
					if(	value.length()>=4) {
						errore=true;
					}
				}catch(StringIndexOutOfBoundsException ex) {
					System.out.println("Errore!1 inserimento sbagliato");
					errore=false;
					
				}
				}while(!errore);
				oldValue = mappa.put(key, value);
				if (oldValue != null) {
					System.out.println("Avviso! La squadra " + oldValue + " associata alla " + key
							+ "� posizione � stato sostituito con " + value);
				}

				do {
					errore = true;
					System.out.print("Vuoi aggiungere altri team ? S/N\n");
					risposta = input.nextLine().toUpperCase();
					if (risposta.equals("S") || (risposta.equals("N"))) {
						errore = false;
					}
				} while (errore);

				if (risposta.equalsIgnoreCase("s") || risposta.equalsIgnoreCase("n"))
					errore = true;
				if (risposta.equalsIgnoreCase("n")) {
					errore = false;
				}

			} catch (NumberFormatException e) {
				System.out.println("Errore! Inserisci un valore positivo intero!\n");
				errore = true;
			}

		} while (errore);
	}

//-------------------------------------------------------------------------------------------//	

	public void getCase2() {

		do {
			if (mappa.size() == 0) {
				System.out.println("Il ranking � vuoto, non ci sono squadre da rimuovere!\n");
				break;
			}
			try {
				System.out.println("Scegli la squadra da eliminare ");
				System.out.println("Squadre disponibili:" + mappa.values());
				risposta = (input.nextLine()).toUpperCase();
				String value = (risposta);

				for (Entry<Integer, String> entry : mappa.entrySet()) {
					if (entry.getValue().equals(value)) {
						System.out.println(entry.getKey());
						key = entry.getKey();
					}
				}

				if (mappa.containsValue(value) == false) {
					System.out.println("Non esiste nessuna squadra chiamata [" + value + "]");
					break;

				} else {
					do {
						errore = true;
						System.out.println("Sicuro di voler rimuovere la squadra? S/N");
						risposta = input.nextLine().toUpperCase();

						if (risposta.equalsIgnoreCase("S") || risposta.equalsIgnoreCase("N")) {
							if (risposta.toLowerCase().charAt(0) == 's') {
								System.out.print("\nEliminazione in corso della squadra");
								for (int i = 0; i < 3; i++) {
									try {
										Thread.sleep(800);
									} catch (Exception e) {
										System.out.println(e);
									}
									System.out.printf(".", (i + 1));
								}
								mappa.remove(key);
								try {
									Thread.sleep(1200);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								System.out.println("\nEliminazione " + value + " completata!\n");
								errore = false;
							} else if (risposta.equalsIgnoreCase("n")) {
								errore = false;
							}
						}
					} while (errore);
				}

				if (mappa.size() == 0) {
					break;
				}

				while (errore) {
					System.out.print("Vuoi eliminare altre squadre ? (S/N) ");
					System.out.println("");
					risposta = input.nextLine().toUpperCase();
					if (risposta.equalsIgnoreCase("S") || risposta.equalsIgnoreCase("N"))
						break;
				}
			} catch (Exception e) {
				System.out.println("La scelta effettuata � sbagliata");
			}
		} while (errore);
	}

//-------------------------------------------------------------------------------------------//	

	public void getCase3() {
		do {
			if (mappa.size() == 0) {
				System.out.println("Il ranking � vuoto, non ci sono squadre da cercare!\n");
				break;
			}
			try {
				do {
					System.out.print("Cerca la squadra in posizione: ");
					scelta = Integer.parseInt(input.nextLine());
					if (scelta < 1)
						System.out.println("Errore! Inserisci un valore positivo! La classifica inizia da 1\n");
				} while (scelta < 1);
				Object value = mappa.get(scelta);
				if (value == null) {
					System.out.println("\nMi dispiace! Nessuna squadra inserita nella " + scelta + "� posizione\n");
				} else {
					System.out.println(scelta + "� posizione: " + value + "\n");
				}
				errore = false;
			} catch (Exception re) {
				System.out.println("\nErrore! Inserisci un valore intero positivo\n");
				errore = true;
			}
		} while (errore);
	}

//-------------------------------------------------------------------------------------------//	

	public void getCase4() {
		if (mappa.size() == 0) {
			System.out.println("Il ranking � vuoto, non c'� nulla da visualizzare!\n");
		} else {
			System.out.println("Grandezza Ranking: [" + mappa.size() + "]\n");
			System.out.println("Ranking:" + mappa.values());
		}
	}

//-------------------------------------------------------------------------------------------//

	public void getCase5() {
		if (mappa.size() == 0) {
			System.out.println("La lista � vuota, non c'� nulla da eliminare!\n");
		} else {
			System.out.print("Eliminazione in corso");
			for (int i = 0; i < 3; i++) {
				try {
					Thread.sleep(800);
				} catch (Exception e) {
					System.out.println(e);
				}
				System.out.printf(".", (i + 1));
			}
			mappa.clear();
			try {
				Thread.sleep(1200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("\nEliminazione completata!\n");
		}
	}

}
